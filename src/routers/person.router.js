import express from 'express';
import * as PersonController from '../controllers/person.controllers.js'

export const router = express.Router()

router.get("/", PersonController.findAll);
router.get("/nameAvailable", PersonController.nameAvailable);
router.get("/:id", PersonController.findById);
router.post("/", PersonController.save);
router.delete("/:id", PersonController.remove);