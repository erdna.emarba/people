import { where } from "sequelize";
import { Address } from "../models/address.js";
import { Person } from "../models/person.js"


export const findAll = async (req, res) => {
    res.json(await Person.findAll());
}

export const findById = async (req, res) => {
    const id = req.params.id;
    const person = await Person.findByPk(id);
    if (person)
        res.json(person);
    else
        res.status(404).json({ message: `no person with id ${id} exists`});
}

export const save = async (req, res) => {
    let person = req.body;
    console.log(JSON.stringify(person));
    const addresses = person.addresses;
    person = await Person.create(person);
    for (const address in addresses)
        await Address.create({ ...address, PersonId: person.id});
    res.json(person);
}

export const remove = async (req, res) => {
    const id = req.params.id;
    const person = await Person.findByPk(id);
    if (person) {
        await person.destroy();
        res.sendStatus(200);
    } else
        res.status(404).json({ message: `no person with id ${id} exists`});
};

export const nameAvailable = async (req, res) => {
    const name = req.query.name;
    const person = await Person.findOne({ where : {
        "lastName": name
    }});
    res.json(person === null);
}
