import { DataTypes } from "sequelize";
import { sequelize } from "../configs/db.config.js";
import { Address } from "./address.js";

export const Person = sequelize.define('Person', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING
});

Person.hasMany(Address);
Address.belongsTo(Person);