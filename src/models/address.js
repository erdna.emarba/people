import { DataTypes } from "sequelize";
import { sequelize } from "../configs/db.config.js";

export const Address = sequelize.define('Address', {
    city: DataTypes.STRING
});