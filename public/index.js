function savePerson(person) {
    return fetch("people", {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(person)
    }).then(res => res.json());
}



document.querySelectorAll("li").forEach(li => {
    li.querySelector('a').addEventListener("click", evt => {
        evt.preventDefault();
        const a = evt.target;
        a.innerText = "deleting ...";
        fetch(a.getAttribute("href"), {method: "delete"}).then(res => {
            if (res.status === 200) {
                a.innerText = "success :)";
                setTimeout(() => li.remove(), 2000);
            } else {
                a.innerText = "error :(";
                setTimeout(() => a.innerText = "delete", 2000);
            }
        })
    })
});

const firstNameInput = document.querySelector("input[name='firstName']")
const lastNameInput = document.querySelector("input[name='lastName']")

function validateFirstName() {
    if (firstNameInput.value.toLowerCase().includes("andre")) {
        firstNameInput.nextElementSibling.innerText = "andre est interdit";
        document.querySelector("#add-button").setAttribute("disabled", true);
    } else {
        firstNameInput.nextElementSibling.innerText = "";
        document.querySelector("#add-button").removeAttribute("disabled");
    }
}

firstNameInput.addEventListener("input", event => {
    validateFirstName();
});

validateFirstName();



let timeout;

function validateLastNameAvailable() {
    timeout = undefined;
    fetch(`http://localhost:3000/people/nameAvailable?name=${lastNameInput.value}`).then(async resp => {
        const available = await resp.json();
        if (!available) {
            lastNameInput.nextElementSibling.innerText = "last name already in used";
            document.querySelector("#add-button").setAttribute("disabled", true);
        } else {
            lastNameInput.nextElementSibling.innerText = "";
            document.querySelector("#add-button").removeAttribute("disabled");
        }
    })
}

lastNameInput.addEventListener("input", event => {
    if (timeout)
        clearTimeout(timeout);
    timeout = setTimeout(validateLastNameAvailable, 500);
})


const socket = io();
socket.onopen = () => {
    socket.send("test message")
}
