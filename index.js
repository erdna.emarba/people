import express from 'express';
import { Person } from './src/models/person.js';
import { Address } from './src/models/address.js';
import { sequelize } from './src/configs/db.config.js';
import { router as PersonRouter } from './src/routers/person.router.js';
import { createServer } from "http";
import { Server } from "socket.io";
import cors from 'cors';


// express initialisation
const app = express();
app.set('view engine', 'hbs');
const server = createServer(app);
const io = new Server(server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"],
    }
  });
// express middleware
app.use(express.json());
app.use(cors());

// registering routes
app.get("/", async (req, res) => {
    res.render("index", {
        people: await Person.findAll()
    })
});
app.use("/people", PersonRouter);
app.use("/public", express.static("public/"))

// syncing database
await sequelize.sync({ alter: true });

// sockets
io.on("connection", (socket) => {
    console.log("new client connected");
});

// starting express
server.listen(3000, () => console.log("app started on port 3000"));
